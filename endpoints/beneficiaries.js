const axios = require('axios');
const buildDefaultFilters = async () => {
    const fromDate = (new Date().getUTCFullYear() - 1) .toString().concat('-01-01');
    const tooDate = new Date().getUTCFullYear().toString().concat('-12-31');
    return {dateType: 'created', fromDate, tooDate };
}
module.exports = {
    async getBeneficiariesByDateRange() {
        let requestBody = await buildDefaultFilters();
        axios({
            method: 'post',
            url: `${process.env.beneficiariesAPI}/dateRange`,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'x-portal-id': '2',
                'X-Identity': `jkkimutai`,
                'Authorization': `Bearer ${process.env.token}`
            },
            data: JSON.stringify(requestBody)
        }).then(response => {
            console.log('-----------BENEFICIARIES RESPONSE-------------------');
            console.log(response.data.header);
            console.log(response.data.body.content);
            console.log('----------------------------------');
        }).catch(error => console.log(error));
    }
};