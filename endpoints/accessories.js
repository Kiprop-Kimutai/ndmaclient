const axios = require('axios');
const fetch = require('node-fetch');
module.exports = {
    fetchAll() {
        axios({
            method: 'get',
            url: `${process.env.accessoriesAPI}/get/packs/all`,
            headers: {
                'Xpt-Encoding': 'application/json',
                'Accept-L-Source-CountryCode': 'KE',
                'X-Source-Operator': 'Safaricom',
                'X-Source-Division': '',
                'X-Source-System': 'mysafaricom-android',
                'X-Source-Timestamp': 'YYYY-MM-DD HH:MM:SS',
                'X-Correlation-ConversationID': '8898899898',
                'X-DeviceInfo': 'safasdfasdfas8df7asdfasd',
                'X-DeviceId': 'asdfasdfasiudfsdfhasdfhjkas',
                'X-DeviceToken': 'asdfasdfasiudfsdfhasdfhjkas',
                'X-MSISDN': '254715109743',
                'X-App': 'safaricomApp',
                'X-Version': 'v1',
                'X-MessageID': 'DNW5gSR9AovUyb9oflO4EgQWrTa2Jawg5A9YDMRjOSU=',
                'Content-Type': 'application/json',
                Accept: 'application/json',
                'x-portal-id': '2',
                'X-Identity': `jkkimutai`,
                Authorization: `Bearer ${process.env.token}`
            }
        }).then(accessoriesreponse => {
            console.log('-------------GET ALL ACCESSORIES------------');
            console.log(accessoriesreponse.data.header);
            console.log(accessoriesreponse.data.body.content);

            console.log('--------------------------------------------');
        }).catch(error => console.log(error));
    }
};