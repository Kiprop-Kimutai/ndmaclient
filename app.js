const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();
const app = express();
app.use(bodyParser.json({urlencoded: false}));
const accessories = require('./endpoints/accessories');
const beneficiaries = require('./endpoints/beneficiaries');
// accessories.fetchAll();
beneficiaries.getBeneficiariesByDateRange();
app.listen(3000, () => {
    console.log('server listening....');
});